<?php
class QAReview
{
    var $sentence = array("source" => "", "sourceId" => "", "target" => "", "comment" => "", "GeneralCommentID" => "");
    
    public function __construct()
    {
        
    }
    
    protected function getTranslation()
    {
        $file = (file_get_contents($this->file));
        $file = str_replace("sdl:cid", "sdl_cid", $file);
        $file = str_replace('sdl:revid', 'sdl_revid', $file);
        $file = str_replace('sdl:cmt', 'sdl_cmt', $file);
        return simplexml_load_string($file);
    }
    
    protected function getTargetTag()
    {
        $startTag        = strpos($this->stringXML, "<target>");
        $finalTag        = strpos($this->stringXML, "</target>");
        $length          = $finalTag - ($startTag);
        $string          = substr($this->stringXML, $startTag, $length + 9);
        $this->stringXML = str_replace($string, "", $this->stringXML);
        $string          = substr($string, 8, strlen($string) - 17);
        return $string;
    }
    
    protected function getTargetSegment()
    {
        $targetTag = $this->getTargetTag();
        $arrayTags = explode("<", $targetTag);
        
        foreach ($arrayTags as $tag) {
            if (strpos($tag, "mtype=\"x-sdl-feedback-deleted\""))
            {
                $startString   = strpos($tag, ">");
                $text          = substr($tag, $startString + 1);
                $returnString .= "<s><font color='red'>$text</font></s>";
            } 
            elseif (strpos($tag, ">") == (strlen($tag) - 1)) 
            {
                $returnString .= "";
            }
            elseif (strpos($tag, "mtype=\"seg\""))
            {
                $startString   = strpos($tag, ">");
                $returnString .= substr($tag, $startString + 1);
            }
            else
            {
                $startString   = strpos($tag, ">");
                $returnString .= substr($tag, $startString + 1);
            }
        }
        return $returnString;
    }
    
    public function getComment()
    {
        $comments[]        = array();
        $this->translation = $this->getTranslation(); 
        foreach ($this->translation->{"doc-info"}->{"cmt-defs"}->{"cmt-def"} as $item)
        {
            foreach ($item->Comments->Comment as $value) 
            {
                $comments[] = array($item[0]['id'],$value);
            }
        }
        
        $comment    = "";
        $delComment = "";
        foreach ($comments as $value) {
            foreach ($this->GeneralCommentID as $com)
            {
                if (trim($value[0]) == trim($com))
                {
                    $comment .= $value[1] . "\n";
                }
            }
            if (trim($value[0]) == trim($this->deleteCommentID) && $this->deleteCommentID != "")
            {
                $delComment .= $value[1] . "\n";
            }
        }
        $generalSpecificCOmment = array($comment,$delComment);
        return $generalSpecificCOmment;
    }
    
    public function getDeleteComment()
    {
        $comments[]        = array();
        $this->translation = $this->getTranslation();

        foreach ($this->translation->{"doc-info"}->{"rev-defs"}->{"rev-def"} as $item)
        {
            if ($item["id"] == trim($this->deleteCommentID))
            {
                $DelID                 = $item->{"sdl_cmt"};
                $this->deleteCommentID = $DelID["id"];
            }
        } 
    }
    
    public function output()
    {
        $returnString      = "";
        $sourceID          = "";
        $this->translation = $this->getTranslation();
        $this->stringXML   = file_get_contents($this->file);
        
        foreach ($this->translation->file->body->group as $translationGroup)
        {
            $this->GeneralCommentID = array();
            $this->sourceID         = $translationGroup->{"trans-unit"}[0]['id'];
            $this->sourceText       = $translationGroup->{"trans-unit"}->source;
            $this->deleteCommentID  = $translationGroup->{"trans-unit"}->target->mrk->mrk[0]['sdl_revid'];
            
            foreach ($translationGroup->{"trans-unit"}->target->mrk->mrk as $key => $value)
            {
                if ($value['sdl_cid'] != null) 
                {
                    $this->GeneralCommentID[] = $value['sdl_cid'];
                }
            }
            $returnString .= $this->getHTML();
        }
        return $returnString;
    }
    
    protected function getHTML()
    {
        $sourceID = $this->sourceID;
        $source   = $this->sourceText;
        $target   = $this->getTargetSegment();
        $this->getDeleteComment();
        $comment  = $this->getComment();
        $general  = "";
        $general  = $comment[0];
        $specific = $comment[1];
        $html = "<tr> 
                        <td> $source </td> 
                        <td> <input class=\"targetText\" id=\"target\" type=\"hidden\" value=\"$target\"/> <div contenteditable=\"true\" class=\"targetSegment\" id=\"openModal\" data-target=\"$target\"data-sourceID=\"$sourceID\" data-source=\"$source\"data-inputFile=\"$this->file\" data-deleteComment=\"$specific\"> $target </td>
                        <td> " . $general . " </td> 
                        <td class=\"controller\">
                            <button type=\"button\" onclick=\"initializeModalAddQuality($(this));\" id=\"openModal\"  data-target=\"$target\" data-sourceID=\"$sourceID\" data-source=\"$source\"data-inputFile=\"$this->file\" data-deleteComment=\"$specific\">
                                     <img src=\"./images/edit.png\"width=\"23\" title=\"Edit\">
                            </button>
                            <button type=\"button\" onclick=\"initializeModalAddGeneralComment($(this));\" id=\"openModal2\" data-comment=\"$general \" data-target=\"$target\" data-GeneralCommentID=\"$GeneralCommentID\" data-sourceID=\"$sourceID\" data-source=\"$source\"data-inputFile=\"$this->file\">
                                    <img src=\"./images/add.png\"width=\"23\" title=\"AddComment\">
                            </button>
                            <button><img src=\"./images/reload.png\"width=\"23\"></button>
                        </td>
                </tr> ";
        return $html;
    }
}
