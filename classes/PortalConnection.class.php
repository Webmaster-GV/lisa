<?php

{

	class PortalConnection
	{

	    protected static $connection;

	    public function getConnection()
	    {     
	        if (!self::$connection)
	        {
	            self::$connection = new PDO('mysql:host=localhost;dbname=globalv_portal_data', 'globalv_netsuite', 'P=[h=x~Mff0z');
	            self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	        }
	        return self::$connection;
	    }
	}
}