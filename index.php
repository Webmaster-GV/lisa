<?php
include $_SERVER['DOCUMENT_ROOT'].'/classes/QAReview.class.php';
include 'classes/ModalFactory.class.php';

//ini_set("display_errors", 1);

$translation = new QAReview();
$translation->file = 'This is a test.docx.sdlxliff';
$modal = new ModalFactory();
?>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
	<div class="container">
		<table width="100%" border="1" align="center">
			<caption>
				<h2>QAReview:</h2>
			</caption>
			<tr>
				<th> Source </th>
				<th> Target </th>
				<th> Comment </th>
				<th> Controller </th>
			</tr>
			<?=$translation->output();?>
		</table>
		<br>
		<div class="modal fade" id="modalAddQuality" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">	
						<h4 class="modal-title">Add Quality Assessment item</h4>
					</div>
					<div class="modal-body">
						<form>
							<fieldset>
								<legend><h2> <img src="images/+.gif" width="19">  Add Quality Assessment item to report translation issues:</h2></legend>
								Category:
								<select name="category">
									<?=$modal->printCategory();?>
								</select>
								Severity:
								<select id="severity" name="severity">
									<option value="Critical">Critical</option>
									<option value="Major">Major</option>
									<option value="Minor">Minor</option>
								</select>
								Type:
								<select>
									<option value="Deletion">Deletion</option>
									<option value="Addition">Addition</option>
									<option value="Replacement">Replacement</option>
									<option value="Comment">Comment</option>
								</select>
								<br>
								<table width="100%">
									<br>
									<td>
										<fieldset>
											<legend><h4>Description mistake:</h4></legend>
											<textarea id="descriptionMistake" rows="4" cols="48" name="Description"></textarea>
										</fieldset>
									</td>
									<td>
										<fieldset>
											<legend><h4>Comment:</h4></legend>
											<textarea rows="4" cols="48" id="Comment" class="comment"></textarea>
										</fieldset>
									</td>
								</table>
								<br>
								<b>Source segment content: </b>
								<br>
								<div class="panel panel-default">
									<div class="panel-body">
										<div id="sourceSegment"></div>
									</div>
								</div>
								<b>Target segment content: </b>
								<br>
								<div class="panel panel-default">
									<div class="panel-body">
										<input="text" id="modalTargetSegment" readonly></div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary"> Previous </button>
									<button type="button" class="btn btn-primary">Next</button>
									<button type="button" class="btn btn-warning btn-cons">Help</button>
									<button type="button" class="btn btn-danger btn-cons" onclick="$('#modalAddQuality').modal('hide');">Cancel</button>
									<button type="button" onclick="upDateSpecificComment();" class="btn btn-success btn-cons">Ok</button>
								</div>
							</fieldset>
							<br>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="addComment" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">	
						<h4 class="modal-title">Add General Comment</h4>
					</div>
					<div class="modal-body">
						<form>
							<textarea rows="10" cols="70" id="generalComment" class="comment"></textarea>
							<br>
						<button type="button" class="btn btn-warning btn-cons">Cancel</button>
							<button align="right" onclick="upDateGeneralComment()"> Ok </button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>

<script type='text/javascript'>

	var source;
	var target;
	var comment;
	var sourceID;
	var generalCommentID;
	
	function initializeModalAddQuality(element,targetIn)
	{	 
		source  	= $(element).attr("data-source");
		comment 	= $(element).attr("data-deleteComment");
		target 		= targetIn;
		sourceID	= $(element).attr("data-sourceID");
		inputFile = $(element).attr("data-inputFile");
		$('#modalTargetSegment').html(target);
		$('#modalAddQuality').modal('show');
		$('#sourceSegment').html(source);
		$('#Comment').val(comment);		
	}
	
	function initializeModalAddGeneralComment(element)
	{		
		comment 		 = $(element).attr("data-comment");
		sourceID		 = $(element).attr("data-sourceID");
		inputFile        = $(element).attr("data-inputFile");
	    generalCommentID = $(element).attr("data-generalCommentID");
	 	$('#generalComment').val(comment);
		$('#addComment').modal('show'); 
	}

	function upDateGeneralComment()
		{
			if( document.getElementById("generalComment").value == comment)
			{
				alert("Comment unmodified");
			}
			else
			{
			 	window.open("addComment.php?comment=" + document.getElementById("generalComment").value + "&commentID=" + generalCommentID + "&user=Gabry&sourceID=" + sourceID + "&inputFile="+inputFile+"&severity=" + document.getElementById("severity").value + "&typeComment=General","title",'scrollbars=no,resizable=yes, width=300,height=100,status=no,location=center, toolbar=no');
			}
		}
		function upDateSpecificComment()
		{
			if( document.getElementById("Comment").value == comment)
			{
				alert("Comment unmodified");
			}
			else
			{
			 	window.open("addComment.php?comment=" + document.getElementById("Comment").value + "&commentID=" + generalCommentID + "&user=Gabry&sourceID=" + sourceID + "&inputFile="+inputFile+"&severity=" + document.getElementById("severity").value + "&typeComment=Delete","title",'scrollbars=no,resizable=yes, width=300,height=100,status=no,location=center, toolbar=no');
			}
		}

		function replaceAll(str, find, replace) {
  		return str.replace(new RegExp(find, 'g'), replace);
		}

	$(".targetSegment").keyup(function(e){

		target = $(this).parent().children(".targetText").val();
		target= target.trim();
		if (e.which == 8 || e.which == 46)
		{
			var textUnmodified = $(this).html();
			textUnmodified	   = textUnmodified.trim();
			textUnmodified	   = replaceAll(textUnmodified,"\"","'");
			var start  		   = -1;
			var finish         = -1;
			var diference      =  0;
			var count          =  0;
			var slice          = "";
	
			for (var i = 0; i <= target.length; i++)
			{
				console.log(target.charAt(i));
				console.log(textUnmodified.charAt(count));

				if(target.charAt(i) == textUnmodified.charAt(count))
				{
					count++;
					if(((textUnmodified.length + diference) == target.length))
					{
						finish = i;
						break;
					}
				}
				else
				{
					diference++;
					if(start == -1)
					{
						start = i;
					}
				}
			}
	      
			for (var i = 0; i <= target.length; i++)
			{
				if(i == start)
				{
					slice += "<s><font color='red'>";
				}
				
				if(i == finish && i != target.length)
				{
					slice += "</font></s>";
				}
				slice += target.charAt(i);
			}
			output = slice;
			$(this).html(output);
			initializeModalAddQuality(this,output);
		}
		
		/*else
		{
			var targetText = $(this).html();
	   		var value = String.fromCharCode(e.keyCode);

	   		if (targetText.slice(targetText.length - 1, targetText.length) == value)
	   		{
	   			value = value;
	   		}
	   		else
	   		{
	   			value = value.toLocaleLowerCase();
	   		}
	   		
	   		positionTag = targetText.indexOf("<font color=\"#00ff00\"");

	   		if (positionTag == -1)
	   		{
	   			output = targetText.slice(0,-1) + value.fontcolor("#00ff00");
	   			$(this).html(output);
	   		}
			else
			{
				output = targetText.slice(0,targetText.length - 8)  + value.fontcolor("#00ff00");
				$(this).html(output);
			}
		}
		*/
	});


</script>